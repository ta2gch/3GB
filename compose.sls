#!r6rs
(library (compose)
  (export compose)
  (import (rnrs base))

  (define (compose . fns)
    (let comp ((fns fns))
      (cond ((null? fns) 'error)
	    ((null? (cdr fns)) (car fns))
	    (else (lambda args
		    (call-with-values
			(lambda ()
			  (apply (comp (cdr fns)) args))
		      (car fns)))))))
  )

