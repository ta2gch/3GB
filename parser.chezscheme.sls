#!r6rs
(library (parser)
  (export parse)
  (import (rnrs base)
	  (rnrs lists)
	  (rnrs control)
	  (variables)
	  (regex))
  
  (define (split str sep)
    (let loop ([s 0] [e 0] [a '()])
      (cond [(> e (string-length str))
	     (reverse (remove "" a))]
	    [(or (= e (string-length str))
		 (char=? (string-ref str e) sep))
	     (loop (+ e 1) (+ e 1) (cons (substring str s e) a))]
	    [else (loop s (+ e 1) a)])))
  
  (define (parse+ s)
    (map parse* (split s #\+)))
  
  (define (parse* s)
    (let* ([c (split s #\*)]
	   [e (map parse^ (cdr c))])
      (let loop ([i 0] [a '()])
	(if (= i (string-length (variables)))
	    (cons (list->vector (reverse a))
		  (string->number (car c)))
	    (let* ([k (string-ref (variables) i)]
		   [v (if (assoc k e) (cdr (assoc k e)) 0)])
	      (loop (+ i 1) (cons v a)))))))
  
  (define (parse^ s)
    (let ([c (split s #\^)])
      (if (= 1 (length c))
	  (cons (string-ref (car c) 0) 1)
	  (cons (string-ref (car c) 0) (string->number (cadr c))))))
  
  (define (parse s)
    (let* ([c (regex-replace-all s "-" "+-")]
	   [d (regex-replace-all c "^([a-z])" "1*$01")]
	   [e (regex-replace-all d "([+-])([a-z])" "$011*$02")])
      (parse+ e)))
  )
