#!r6rs
(library (monomial)
  (export m* m/ m=)
  (import (rnrs))
  
  (define (m= m n) (equal? m n))
  (define (m* m n) (vector-map + m n))
  (define (m/ m n) (vector-map - m n))
  )
