#!r6rs
(library (groebner)
  (export groebner groebner+opt)
  (import (except (rnrs) lcm)
	  (polynomial)
	  (monomial)
	  (term)
	  (compose)
	  (ordering)
	  (util)
	  (printer)
	  (division))
  
  (define (lcm m n)
    (vector-map max m n))
  
  (define (syxyzy f g)
    (let* ([t (monomial->term (lcm (lm f) (lm g)))]
	   [p (term->polynomial (t/ t (lt f)))]
	   [q (term->polynomial (t/ t (lt g)))])
      (p- (p* p f) (p* q g))))

  (define (minimalize . g)
    (let loop ([fut g] [pat '()])
      (if (null? fut)
	  (map (lambda (p)
		 (p* (term->polynomial (number->term (/ 1 (lc p)))) p))
	       pat)
	  (let ([dividend (term->polynomial (lt (car fut)))]
		[divisors (map (compose term->polynomial lt)
			       (append (cdr fut) pat))])
	    (if (null? (apply remainder  dividend divisors))
		(loop (cdr fut) pat)
		(loop (cdr fut) (cons (car fut) pat)))))))

  (define (strictify g r)
    (let ([s (apply remainder r g)])
      (if (null? s)
	  s
	  (p+ (term->polynomial (lt s))
	      (strictify g (p- s (term->polynomial (lt s))))))))
  
  (define (reduce . f)
    (let loop ([g f] [h '()])
      (if (null? g)
	  (map (lambda (p)
		 (p* (term->polynomial (number->term (/ 1(lc p)))) p))
	       h)
	  (let* ([t (remp (lambda (p) (p= p (car g))) f)]
		 [s (strictify t (apply remainder (car g) t))])
	    (loop (cdr g) (cons s h))))))
  
  (define (groebner . f)
    (define (proc p q)
      (or (p= p q) (apply remainder (syxyzy p q) f)))
    (define (pred p)
      (or (not (pair? p)) (memp (lambda (q) (p= p q)) f)))
    (let ([rs (unique (remp pred (map-combination proc f f)))])
      (if (zero? (length rs))
      	  (apply reduce (apply minimalize f))
	  (apply groebner (append f rs)))))

  (define (groebner+opt . f)
    (define (base p q f)
      (or (p= p q)
	  (m= (m* (lm p) (lm q)) (lcm (lm p) (lm q)))
	  (apply remainder (syxyzy p q) f)))
    (define (basis f g)
      (if (null? g) f
	  (let* ([h (append f g)]
		 [pred (lambda (p)
			 (or (not (pair? p))
			     (memp (lambda (q) (p= p q)) h)))]
		 [basis1 (map-combination base f g (list h))]
		 [basis2 (map-combination base g g (list h))]
		 [basis3 (unique (remp pred (append basis1 basis2)))])
	    (basis h basis3))))
    (let* ([pred (lambda (p)
		   (or (not (pair? p))
		       (memp (lambda (q) (p= p q)) f)))]
	   [basis1 (map-combination base f f (list f))]
	   [basis2 (unique (remp pred basis1))])
      (apply reduce (apply minimalize (basis f basis2)))))
  )

