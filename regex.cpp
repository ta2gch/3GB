#include <scheme.h>
#include <regex>

EXPORT ptr regex_replace(char* s, char* e, char* fmt, int flags) {
  auto regex = std::regex(e);
  auto once = flags
    ? std::regex_constants::format_first_only
    : std::regex_constants::match_default;
  auto result = std::regex_replace(s, regex, fmt, once);
  return Sstring(result.c_str());
}

